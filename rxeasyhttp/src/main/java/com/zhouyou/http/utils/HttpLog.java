/*
 * Copyright (C) 2017 zhouyou(478319399@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zhouyou.http.utils;


import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class HttpLog {
    public  String customTagPrefix = "RxEasyHttp_";
    private static final String TAG = "HttpLog";
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);
    private static HttpLog singleton;

    public HttpLog setCustomTagPrefix(String customTagPrefix) {
        this.customTagPrefix = customTagPrefix;
        return this;
    }

    public String getCustomTagPrefix() {
        return customTagPrefix;
    }

    private HttpLog() {

    }

    public static HttpLog getInstance() {
        if (singleton == null) {
            synchronized (HttpLog.class) {
                if (singleton == null) {
                    singleton = new HttpLog();
                }
            }
        }
        return singleton;
    }

    private boolean allowD = true;
    private boolean allowE = true;

    private boolean allowV = true;
    private boolean allowW = true;
    private boolean allowI = true;
    private boolean allowWtf = true;

    public boolean isAllowD() {

        return allowD;
    }

    public boolean isAllowE() {

        return allowE;
    }

    public boolean isAllowI() {

        return allowI;
    }

    public boolean isAllowV() {

        return allowV;
    }

    public boolean isAllowW() {

        return allowW;
    }

    public boolean isAllowWtf() {

        return allowWtf;
    }

    public HttpLog setAllowD(boolean allowD) {
        this.allowD = allowD;
        return this;
    }

    public HttpLog setAllowE(boolean allowE) {
        this.allowE = allowE;
        return this;
    }

    public HttpLog setAllowV(boolean allowV) {
        this.allowV = allowV;
        return this;
    }

    public HttpLog setAllowW(boolean allowW) {
        this.allowW = allowW;
        return this;
    }

    public HttpLog setAllowI(boolean allowI) {
        this.allowI = allowI;
        return this;
    }

    public HttpLog setAllowWtf(boolean allowWtf) {
        this.allowWtf = allowWtf;
        return this;
    }

    private String generateTag(StackTraceElement caller) {
        String tag = "%s.%s(L:%d)";
        String callerClazzName = caller.getClassName();
        callerClazzName = callerClazzName.substring(callerClazzName.lastIndexOf(".") + 1);
        tag = String.format(tag, callerClazzName, caller.getMethodName(), caller.getLineNumber());
        tag = TextUtils.isEmpty(customTagPrefix) ? tag : customTagPrefix + ":" + tag;
        return tag;
    }

    public final CustomLogger customLogger = null;

    public interface CustomLogger {
        void d(String tag, String content);

        void d(String tag, String content, Throwable tr);

        void e(String tag, String content);

        void e(String tag, String content, Throwable tr);

        void i(String tag, String content);

        void i(String tag, String content, Throwable tr);

        void v(String tag, String content);

        void v(String tag, String content, Throwable tr);

        void w(String tag, String content);

        void w(String tag, String content, Throwable tr);

        void w(String tag, Throwable tr);

        void wtf(String tag, String content);

        void wtf(String tag, String content, Throwable tr);

        void wtf(String tag, Throwable tr);
    }

    public void d(String content) {
        if (!isAllowD()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.d(tag, content);
        } else {

            HiLog.debug(LABEL, content);
        }
    }

    public StackTraceElement getCallerStackTraceElement() {
        return Thread.currentThread().getStackTrace()[4];
    }

    public void d(String content, Throwable tr) {
        if (!isAllowD()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.d(tag, content, tr);
        } else {

            HiLog.debug(LABEL, content, tr);
        }
    }

    public void e(String content) {
        if (!isAllowE()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.e(tag, content);
        } else {

            HiLog.error(LABEL, content);
        }
    }

    public void e(Exception e) {
        if (!isAllowE()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.e(tag, e.getMessage(), e);
        } else {
            HiLog.error(LABEL, e.getMessage(), e);
        }
    }

    public void e(String content, Throwable tr) {
        if (!isAllowE()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.e(tag, content, tr);
        } else {
            HiLog.error(LABEL, content, tr);
        }
    }

    public void i(String content) {
        if (!isAllowI()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.i(tag, content);
        } else {
            HiLog.info(LABEL, content);
        }
    }

    public void i(String content, Throwable tr) {
        if (!isAllowI()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.i(tag, content, tr);
        } else {
            HiLog.info(LABEL, content, tr);
        }
    }

    public void v(String content) {
        if (!isAllowV()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.v(tag, content);
        } else {
            HiLog.info(LABEL, content);
        }
    }

    public void v(String content, Throwable tr) {
        if (!isAllowV()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.v(tag, content, tr);
        } else {

            HiLog.info(LABEL, content, tr);
        }
    }

    public void w(String content) {
        if (!isAllowW()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.w(tag, content);
        } else {

            HiLog.warn(LABEL, content);
        }
    }

    public void w(String content, Throwable tr) {
        if (!isAllowW()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.w(tag, content, tr);
        } else {

            HiLog.warn(LABEL, content, tr);
        }
    }

    public void w(Throwable tr) {
        if (!isAllowW()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.w(tag, tr);
        } else {

            HiLog.warn(LABEL, tr.toString());
        }
    }


    public void wtf(String content) {
        if (!isAllowWtf()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.wtf(tag, content);
        } else {

            HiLog.fatal(LABEL, content);
        }
    }

    public void wtf(String content, Throwable tr) {
        if (!isAllowWtf()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.wtf(tag, content, tr);
        } else {

            HiLog.fatal(LABEL, content, tr);
        }
    }

    public void wtf(Throwable tr) {
        if (!isAllowWtf()) return;
        StackTraceElement caller = getCallerStackTraceElement();
        String tag = generateTag(caller);

        if (customLogger != null) {
            customLogger.wtf(tag, tr);
        } else {
            HiLog.fatal(LABEL, tr.toString());
        }
    }

}
