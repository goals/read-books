package com.talkweb.reader.bookshelf;

import com.talkweb.baselibrary.utils.PermissionsUtils;
import com.talkweb.baselibrary.utils.ToastHelper;
import com.talkweb.reader.bookshelf.slice.BookShelfSlice;
import com.talkweb.reader.slice.MainAbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

public class BookShelfAbility extends FractionAbility implements PermissionsUtils.IPermissionsResult{
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(BookShelfSlice.class.getName());
        initPermissions();
    }

    private void initPermissions() {
        PermissionsUtils.getInstance().chekPermissions(this,
                PermissionsUtils.storagePermissions, this);
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        PermissionsUtils.getInstance().onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    @Override
    public void passPermissons() {
        ToastHelper.showToastText(this, "GotPermissions!");
    }

    @Override
    public void forbitPermissons() {
        ToastHelper.showToastText(this, "PermissionsDenied!");
        terminateAbility();
    }
}
