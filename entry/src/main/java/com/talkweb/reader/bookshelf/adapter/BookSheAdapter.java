package com.talkweb.reader.bookshelf.adapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.talkweb.reader.ResourceTable;
import com.talkweb.reader.bookshelf.bean.BookBean;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

public class BookSheAdapter extends BaseItemProvider {

    private int mhotCount = 1;//最热数量
    private int mRecommendCount = 1;//推荐的数量
    private List<BookBean> hotRanking;
    private List<BookBean> recommend;
    private List<BookBean> contentList;

    // 首先定义几个常量标记item的类型
    private static final int ITEM_TYPE_HOT = 0;
    private static final int ITEM_TYPE_CONTENT = 1;
    private static final int ITEM_TYPE_RECOMMEND = 2;

    private Context context;
    //中间内容位置信息
    private int mContentPosition;

    //各种点击时间接口,实现在fragment中
    private OnBookShelfAdapterClickListener listener;

    public void setNewData(List<BookBean> hotRanking,List<BookBean> recommend,List<BookBean> contentList){
        this.hotRanking = hotRanking;
        this.recommend = recommend;
        this.contentList = contentList;
        if(this.hotRanking!=null){
            if(this.hotRanking.size()%2 == 0){
                this.mhotCount = this.hotRanking.size()/2;
            }else{
                this.mhotCount = this.hotRanking.size()/2 +1;
            }
        }
        notifyDataChanged();
    }

    public BookSheAdapter(Context context, List<BookBean> hotRanking, List<BookBean> recommend, List<BookBean> contentList ,
                          OnBookShelfAdapterClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.hotRanking = hotRanking;
        this.recommend = recommend;
        this.contentList = contentList;
        if(this.hotRanking!=null){
            if(this.hotRanking.size()%2 == 0){
                this.mhotCount = this.hotRanking.size()/2;
            }else{
                this.mhotCount = this.hotRanking.size()/2 +1;
            }
        }
    }

    // 底部内容长度
    private int getContentItemCount() {
        return contentList.size();
    }

    // 判断当前item是否是最热头部（根据position来判断）
    private boolean isHotView(int position) {
        return mhotCount != 0 && position <= mhotCount && position!=0;
    }

    // 判断当前item是否为经典推荐位
    private boolean isRecommendView(int position) {
        return mRecommendCount != 0 && position ==0;
    }

    @Override
    public int getCount() {
        return contentList.size() + mhotCount + mRecommendCount;
    }

    @Override
    public Object getItem(int position) {
        return contentList.get(position - mhotCount - mRecommendCount);
    }

    @Override
    public long getItemId(int position) {
        return position - mhotCount - mRecommendCount;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        HotHolder headViewHolder = null;
        RecommendViewHolder recommendViewHolder = null;
        ListViewHolder listViewHolder = null;
        int type = getItemComponentType(position);
        mContentPosition = position - mhotCount - mRecommendCount;
        if (component == null) {
            switch (type) {
                case ITEM_TYPE_HOT:
                    component = LayoutScatter.getInstance(context).parse(
                            ResourceTable.Layout_book_shelf_item_hot, componentContainer, false);
                    headViewHolder = new HotHolder();
                    headViewHolder.titleLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_titleLayout);
                    headViewHolder.lay1 = (DependentLayout) component.findComponentById(ResourceTable.Id_firstLayout);
                    headViewHolder.lay2 = (DependentLayout) component.findComponentById(ResourceTable.Id_twoLayout);
                    headViewHolder.image1 = (Image) component.findComponentById(ResourceTable.Id_mp_home_hot_firstImage);
                    headViewHolder.image2 = (Image) component.findComponentById(ResourceTable.Id_mp_home_hot_twoImage);
                    headViewHolder.title1 = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_fristText);
                    headViewHolder.num1 = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_fristText2);
                    headViewHolder.title2 = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_twoText);
                    headViewHolder.num2 = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_twoText2);
                    component.setTag(headViewHolder);
                    break;
                case ITEM_TYPE_RECOMMEND:
                    component = LayoutScatter.getInstance(context).parse(
                            ResourceTable.Layout_book_shelf_item_recommend, componentContainer, false);
                    recommendViewHolder = new RecommendViewHolder();
                    recommendViewHolder.firstLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_firstLayout);
                    recommendViewHolder.twoLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_twoLayout);
                    recommendViewHolder.threeLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_threeLayout);
                    recommendViewHolder.mp_home_recommend_firstImage = (Image) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_firstImage);
                    recommendViewHolder.mp_home_recommend_twoImage = (Image) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_twoImage);
                    recommendViewHolder.mp_home_recommend_threeImage = (Image) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_threeImage);
                    recommendViewHolder.mp_home_recommend_fristText = (Text) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_fristText);
                    recommendViewHolder.mp_home_recommend_twoText = (Text) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_twoText);
                    recommendViewHolder.mp_home_recommend_threeText = (Text) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_threeText);
                    component.setTag(recommendViewHolder);
                    break;
                case ITEM_TYPE_CONTENT:
                    component = LayoutScatter.getInstance(context).parse(
                            ResourceTable.Layout_book_shelf_item_content, componentContainer, false);
                    listViewHolder = new ListViewHolder();
                    listViewHolder.cardTitleImage = (Image) component.findComponentById(ResourceTable.Id_mp_home_hot_firstImage);
                    listViewHolder.cardFristText = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_fristText);
                    listViewHolder.cardTowText = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_fristText2);
                    listViewHolder.cardThreeText = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_fristText3);
                    listViewHolder.cardFourText = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_fristText4);
                    listViewHolder.titleLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_titleLayout);
                    listViewHolder.firstLayout = (DependentLayout) component.findComponentById(ResourceTable.Id_firstLayout);
                    listViewHolder.rating = (Rating) component.findComponentById(ResourceTable.Id_rating);
                    component.setTag(listViewHolder);
                    break;
            }
        } else {
            switch (type) {
                case ITEM_TYPE_HOT:
                    headViewHolder = (HotHolder) component.getTag();
                    break;
                case ITEM_TYPE_RECOMMEND:
                    recommendViewHolder = (RecommendViewHolder) component.getTag();
                    break;
                case ITEM_TYPE_CONTENT:
                    listViewHolder = (ListViewHolder) component.getTag();
                    break;
            }
        }

        switch (type) {
            case ITEM_TYPE_HOT:
                headViewHolder.lay1.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        listener.onItemClickListener(component,hotRanking.get(2*(position-1)));
                    }
                });
                Glide.with(context)
                        .load(hotRanking.get(2*(position-1)).getCoverUrl())
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .into(headViewHolder.image1);
                headViewHolder.title1.setText(hotRanking.get(2*(position-1)).getName());
                headViewHolder.num1.setText(hotRanking.get(2*(position-1)).getSearCount()+"万热度");
                if(hotRanking.size()>=position*2){
                    headViewHolder.lay2.setVisibility(Component.VISIBLE);
                    Glide.with(context)
                            .load(hotRanking.get(2*(position-1)+1).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(headViewHolder.image2);
                    headViewHolder.title2.setText(hotRanking.get(2*(position-1)+1).getName());
                    headViewHolder.num2.setText(hotRanking.get(2*(position-1)+1).getSearCount()+"万热度");
                    headViewHolder.lay2.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component,hotRanking.get(2*(position-1)+1));
                        }
                    });
                }else{
                    headViewHolder.lay2.setVisibility(Component.INVISIBLE);

                }
                if(position == 1){
                    headViewHolder.titleLayout.setVisibility(Component.VISIBLE);
                }else{
                    headViewHolder.titleLayout.setVisibility(Component.HIDE);
                }
                break;
            case ITEM_TYPE_RECOMMEND:
                if(recommend.size() > 0){
                    Glide.with(context)
                            .load(recommend.get(0).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(recommendViewHolder.mp_home_recommend_firstImage);
                    recommendViewHolder.mp_home_recommend_fristText.setText(recommend.get(0).getName());
                    recommendViewHolder.firstLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component,recommend.get(0));
                        }
                    });
                }
                if(recommend.size() > 1){
                    Glide.with(context)
                            .load(recommend.get(1).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(recommendViewHolder.mp_home_recommend_twoImage);
                    recommendViewHolder.mp_home_recommend_twoText.setText(recommend.get(1).getName());
                    recommendViewHolder.twoLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component,recommend.get(1));
                        }
                    });

                }
                if(recommend.size() > 2){
                    Glide.with(context)
                            .load(recommend.get(2).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(recommendViewHolder.mp_home_recommend_threeImage);
                    recommendViewHolder.mp_home_recommend_threeText.setText(recommend.get(2).getName());
                    recommendViewHolder.threeLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component,recommend.get(2));
                        }
                    });
                }
                break;
            case ITEM_TYPE_CONTENT:
                if(contentList.size() > 0){
                    BookBean homeDesignBean = contentList.get(mContentPosition);
                    Glide.with(context)
                            .load(homeDesignBean.getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardTitleImage);
                    listViewHolder.cardFristText.setText(homeDesignBean.getName());
                    listViewHolder.cardTowText.setText(homeDesignBean.getBookdesc());
                    listViewHolder.cardThreeText.setText(homeDesignBean.getAuthor());
                    listViewHolder.cardFourText.setText(homeDesignBean.getKind());
                    if(mContentPosition == 0){
                        listViewHolder.titleLayout.setVisibility(Component.VISIBLE);
                    }else{
                        listViewHolder.titleLayout.setVisibility(Component.HIDE);
                    }
                    listViewHolder.firstLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component,homeDesignBean);
                        }
                    });
                }

                break;
        }

        return component;
    }

    @Override
    public int getItemComponentType(int position) {
        if (isHotView(position)) {
            // 最热View
            return ITEM_TYPE_HOT;
        } else if (isRecommendView(position)) {
            return ITEM_TYPE_RECOMMEND;
        } else {
            // 内容View
            return ITEM_TYPE_CONTENT;
        }
    }

    public class HotHolder {
        DirectionalLayout titleLayout;
        DependentLayout lay1;
        DependentLayout lay2;
        Image image1;
        Image image2;
        Text title1;
        Text title2;
        Text num1;
        Text num2;
    }

    public class RecommendViewHolder {
        DirectionalLayout firstLayout;
        DirectionalLayout twoLayout;
        DirectionalLayout threeLayout;
        Image mp_home_recommend_firstImage;
        Image mp_home_recommend_twoImage;
        Image mp_home_recommend_threeImage;
        Text mp_home_recommend_fristText;
        Text mp_home_recommend_twoText;
        Text mp_home_recommend_threeText;
    }

    public class ListViewHolder {
        Image cardTitleImage;

        Text cardFristText;
        Text cardTowText;
        Text cardThreeText;
        Text cardFourText;
        DependentLayout firstLayout;
        DirectionalLayout titleLayout;
        Rating rating;
    }
}


