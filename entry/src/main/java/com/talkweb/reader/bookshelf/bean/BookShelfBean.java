/**
 * Copyright 2020 bejson.com
 */
package com.talkweb.reader.bookshelf.bean;

import com.talkweb.reader.maintab.bean.SourceListContent;

import java.util.List;

/**
 * 书架内容JavaBean解析类
 */
public class BookShelfBean {

    private List<BookBean> hotRanking;//最热
    private List<BookBean> recommend;//推荐
    private List<BookBean> contentList;//内容
    private List<BookBean> moreRecommendList;//更多分页内容

    public List<BookBean> getMoreRecommendList() {
        return moreRecommendList;
    }

    public void setMoreRecommendList(List<BookBean> moreRecommendList) {
        this.moreRecommendList = moreRecommendList;
    }

    public void addMoreRecommendList(List<BookBean> moreRecommendList) {
        this.moreRecommendList.addAll(moreRecommendList);
    }

    public List<BookBean> getHotRanking() {
        return hotRanking;
    }

    public void setHotRanking(List<BookBean> hotRanking) {
        this.hotRanking = hotRanking;
    }

    public List<BookBean> getRecommend() {
        return recommend;
    }

    public void setRecommend(List<BookBean> recommend) {
        this.recommend = recommend;
    }

    public List<BookBean> getContentList() {
        return contentList;
    }

    public void setContentList(List<BookBean> contentList) {
        this.contentList = contentList;
    }
}