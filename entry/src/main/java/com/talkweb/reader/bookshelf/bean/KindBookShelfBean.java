/**
 * Copyright 2020 bejson.com
 */
package com.talkweb.reader.bookshelf.bean;

import java.util.List;

/**
 * 书架内容JavaBean解析类
 */
public class KindBookShelfBean {
    private List<BookBean> sourceListContent;//内容
    private String kind;
    private String cardColor;

    public List<BookBean> getSourceListContent() {
        return sourceListContent;
    }

    public void setSourceListContent(List<BookBean> sourceListContent) {
        this.sourceListContent = sourceListContent;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getCardColor() {
        return cardColor;
    }

    public void setCardColor(String cardColor) {
        this.cardColor = cardColor;
    }
}