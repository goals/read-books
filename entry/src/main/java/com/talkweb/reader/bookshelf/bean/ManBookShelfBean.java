/**
 * Copyright 2020 bejson.com
 */
package com.talkweb.reader.bookshelf.bean;

import java.util.List;

/**
 * 书架内容JavaBean解析类
 */
public class ManBookShelfBean {

    private List<BookBean> hotRanking;//最热
    private List<BookBean> recommend;//推荐
    private List<KindBookShelfBean> contentList;//内容

    public List<BookBean> getHotRanking() {
        return hotRanking;
    }

    public void setHotRanking(List<BookBean> hotRanking) {
        this.hotRanking = hotRanking;
    }

    public List<BookBean> getRecommend() {
        return recommend;
    }

    public void setRecommend(List<BookBean> recommend) {
        this.recommend = recommend;
    }

    public List<KindBookShelfBean> getContentList() {
        return contentList;
    }

    public void setContentList(List<KindBookShelfBean> contentList) {
        this.contentList = contentList;
    }
}