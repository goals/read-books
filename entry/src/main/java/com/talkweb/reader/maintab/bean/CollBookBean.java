package com.talkweb.reader.maintab.bean;


import com.talkweb.baselibrary.utils.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * 收藏的书籍
 */
public class CollBookBean implements Serializable {

    public static final int STATUS_UNCACHE = 0; //未缓存
    public static final int STATUS_CACHING = 1; //正在缓存
    public static final int STATUS_CACHED = 2;  //已经缓存
    /**
     * _id : 53663ae356bdc93e49004474
     * title : 逍遥派
     * author : 白马出淤泥
     * shortIntro : 金庸武侠中有不少的神秘高手，书中或提起名字，或不曾提起，总之他们要么留下了绝世秘笈，要么就名震武林。 独孤九剑的创始者，独孤求败，他真的只创出九剑吗？ 残本葵花...
     * cover : /cover/149273897447137
     * hasCp : true
     * latelyFollower : 60213
     * retentionRatio : 22.87
     * updated : 2017-05-07T18:24:34.720Z
     * <p>
     * chaptersCount : 1660
     * lastChapter : 第1659章 朱长老
     */
    private String _id; // 本地书籍中，path 的 md5 值作为本地书籍的 id
    private String title;//书名
    private String author;//作者
    private String shortIntro;//图书简介
    private String cover; // 在本地书籍中，该字段作为本地文件的路径//网络图书为图片
    private boolean hasCp;
    private int latelyFollower;
    private double retentionRatio;
    //最新更新日期 //必填项
    private String updated;
    //最新阅读日期//必填项
    private String lastRead;
    private int chaptersCount;
    private String lastChapter;
    //是否更新或未阅读
    private boolean isUpdate = true;
    //是否是本地文件
    private boolean isLocal = false;
    //数据源地址标识
    private String bookTag;
    private String bookChapterUrl;

    public String getBookChapterUrl() {
        return bookChapterUrl;
    }

    public void setBookChapterUrl(String bookChapterUrl) {
        this.bookChapterUrl = bookChapterUrl;
    }

    private List<BookChapterBean> bookChapterList;
    /**
     * Used to resolve relations
     */
    /**
     * Used for active entity operations.
     */

    public CollBookBean(String _id, String title, String author, String shortIntro, String cover, boolean hasCp,
                        int latelyFollower, double retentionRatio, String updated, String lastRead, int chaptersCount,
                        String lastChapter, boolean isUpdate, boolean isLocal, String bookTag, String bookChapterUrl) {
        this._id = _id;
        this.title = title;
        this.author = author;
        this.shortIntro = shortIntro;
        this.cover = cover;
        this.hasCp = hasCp;
        this.latelyFollower = latelyFollower;
        this.retentionRatio = retentionRatio;
        this.updated = updated;
        this.lastRead = lastRead;
        this.chaptersCount = chaptersCount;
        this.lastChapter = lastChapter;
        this.isUpdate = isUpdate;
        this.isLocal = isLocal;
        this.bookTag = bookTag;
        this.bookChapterUrl = bookChapterUrl;
    }

    public CollBookBean() {
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getShortIntro() {
        return shortIntro;
    }

    public void setShortIntro(String shortIntro) {
        this.shortIntro = shortIntro;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public boolean isHasCp() {
        return hasCp;
    }

    public void setHasCp(boolean hasCp) {
        this.hasCp = hasCp;
    }

    public int getLatelyFollower() {
        return latelyFollower;
    }

    public void setLatelyFollower(int latelyFollower) {
        this.latelyFollower = latelyFollower;
    }

    public double getRetentionRatio() {
        return retentionRatio;
    }

    public void setRetentionRatio(double retentionRatio) {
        this.retentionRatio = retentionRatio;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public int getChaptersCount() {
        return chaptersCount;
    }

    public void setChaptersCount(int chaptersCount) {
        this.chaptersCount = chaptersCount;
    }

    public String getLastChapter() {
        return lastChapter;
    }

    public void setLastChapter(String lastChapter) {
        this.lastChapter = lastChapter;
    }

    public boolean isUpdate() {
        return isUpdate;
    }

    public void setUpdate(boolean update) {
        isUpdate = update;
    }

    public boolean getHasCp() {
        return this.hasCp;
    }

    public boolean getIsUpdate() {
        return this.isUpdate;
    }

    public void setIsUpdate(boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    public boolean isLocal() {
        return isLocal;
    }

    public void setLocal(boolean local) {
        isLocal = local;
    }

    public String getLastRead() {
        return lastRead;
    }

    public String getBookTag() {
        return bookTag;
    }

    public void setBookTag(String bookTag) {
        this.bookTag = bookTag;
    }

    public void setLastRead(String lastRead) {
        this.lastRead = lastRead;
    }



    public boolean getIsLocal() {
        return this.isLocal;
    }

    public void setIsLocal(boolean isLocal) {
        this.isLocal = isLocal;
    }




    /**
     * searchBook转换为CollBook
     *
     * @param searchBookBean
     * @return
     */
    public CollBookBean getCollBookBeanFromSearch(SearchBookBean searchBookBean) {
        CollBookBean collBookBean = new CollBookBean();
        collBookBean.set_id(searchBookBean.getNoteUrl());
        collBookBean.setAuthor(searchBookBean.getAuthor());
        collBookBean.setCover(searchBookBean.getCoverUrl());
        collBookBean.setIsLocal(false);
        collBookBean.setIsUpdate(false);
        collBookBean.setTitle(searchBookBean.getName());
        collBookBean.setLastRead(StringUtils.getTime(System.currentTimeMillis()));
        if (searchBookBean.getLastChapter() != null) {
            collBookBean.setLastChapter(searchBookBean.getLastChapter());
        } else {
            collBookBean.setLastChapter("无章节");
        }

        collBookBean.setHasCp(false);
        if (searchBookBean.getUpdated() != null && searchBookBean.getUpdated().length() > 0) {
            collBookBean.setUpdated(searchBookBean.getUpdated());
        } else {
            collBookBean.setUpdated(StringUtils.getTime(System.currentTimeMillis()));
        }
        if (StringUtils.isEmpty(searchBookBean.getDesc())) {
            collBookBean.setShortIntro("暂无介绍");
        } else {
            collBookBean.setShortIntro(searchBookBean.getDesc());
        }
        collBookBean.setBookTag(searchBookBean.getTag());
        return collBookBean;
    }
}