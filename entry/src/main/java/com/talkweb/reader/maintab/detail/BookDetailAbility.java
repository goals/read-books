package com.talkweb.reader.maintab.detail;

import com.talkweb.baselibrary.base.BaseAbility;
import com.talkweb.reader.slice.MainAbilitySlice;
import ohos.aafwk.content.Intent;

public class BookDetailAbility extends BaseAbility {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(BookDetailAbilitySlice.class.getName());
    }
}
