package com.talkweb.reader.maintab.adapter;


import com.talkweb.reader.maintab.bean.SourceListContent;
import ohos.agp.components.Component;

public interface OnHomeAdapterClickListener {
    void onItemClickListener(Component component);

    void onLayoutClickListener(Component component, SourceListContent sourceListContent);

    void onContentChangeClickListener(int mContentPosition,String kinds);
}
