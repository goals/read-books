package com.talkweb.reader.book.adapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.talkweb.reader.ResourceTable;
import com.talkweb.reader.bookshelf.adapter.OnBookShelfAdapterClickListener;
import com.talkweb.reader.bookshelf.bean.BookBean;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

public class BookMySheAdapter extends BaseItemProvider {

    private List<BookBean> contentList;

    // 首先定义几个常量标记item的类型
    private static final int ITEM_TYPE_CONTENT = 1;
    private static final int ITEM_TYPE_NEW = 2;

    private Context context;

    //各种点击时间接口,实现在fragment中
    private OnBookShelfAdapterClickListener listener;

    public void setNewData(List<BookBean> contentList){
        this.contentList = contentList;
        notifyDataChanged();
    }

    public BookMySheAdapter(Context context,List<BookBean> contentList ,
                            OnBookShelfAdapterClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.contentList = contentList;
    }

    @Override
    public int getCount() {
        if(contentList.size()>1){
            return (int)Math.ceil((float)(contentList.size()-1)/3)+1;
        }else if(contentList.size()<=1){
            return contentList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        RecommendViewHolder recommendViewHolder = null;
        ListViewHolder listViewHolder = null;
        int type = getItemComponentType(position);
        if (component == null) {
            switch (type) {
                case ITEM_TYPE_NEW:
                    component = LayoutScatter.getInstance(context).parse(
                            ResourceTable.Layout_book_shelf_item_content, componentContainer, false);
                    listViewHolder = new ListViewHolder();
                    listViewHolder.cardTitleImage = (Image) component.findComponentById(ResourceTable.Id_mp_home_hot_firstImage);
                    listViewHolder.cardFristText = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_fristText);
                    listViewHolder.cardTowText = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_fristText2);
                    listViewHolder.cardThreeText = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_fristText3);
                    listViewHolder.cardFourText = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_fristText4);
                    listViewHolder.titleLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_titleLayout);
                    listViewHolder.firstLayout = (DependentLayout) component.findComponentById(ResourceTable.Id_firstLayout);
                    listViewHolder.rating = (Rating) component.findComponentById(ResourceTable.Id_rating);
                    component.setTag(listViewHolder);

                    break;
                case ITEM_TYPE_CONTENT:
                    component = LayoutScatter.getInstance(context).parse(
                            ResourceTable.Layout_book_shelf_item, componentContainer, false);
                    recommendViewHolder = new RecommendViewHolder();
                    recommendViewHolder.bookLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_bookLayout);
                    recommendViewHolder.firstLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_firstLayout);
                    recommendViewHolder.twoLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_twoLayout);
                    recommendViewHolder.threeLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_threeLayout);
                    recommendViewHolder.mp_home_recommend_firstImage = (Image) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_firstImage);
                    recommendViewHolder.mp_home_recommend_twoImage = (Image) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_twoImage);
                    recommendViewHolder.mp_home_recommend_threeImage = (Image) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_threeImage);
                    recommendViewHolder.mp_home_recommend_fristText = (Text) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_fristText);
                    recommendViewHolder.mp_home_recommend_twoText = (Text) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_twoText);
                    recommendViewHolder.mp_home_recommend_threeText = (Text) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_threeText);
                    recommendViewHolder.mp_home_recommend_fristText.setVisibility(Component.HIDE);
                    recommendViewHolder.mp_home_recommend_twoText.setVisibility(Component.HIDE);
                    recommendViewHolder.mp_home_recommend_threeText.setVisibility(Component.HIDE);
                    component.setTag(recommendViewHolder);
                    break;
            }
        } else {
            switch (type) {
                case ITEM_TYPE_NEW:
                    recommendViewHolder = (RecommendViewHolder) component.getTag();
                    break;
                case ITEM_TYPE_CONTENT:
                    listViewHolder = (ListViewHolder) component.getTag();
                    break;
            }
        }

        switch (type) {
            case ITEM_TYPE_NEW:
                BookBean homeDesignBean = contentList.get(0);
                Glide.with(context)
                        .load(homeDesignBean.getCoverUrl())
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .into(listViewHolder.cardTitleImage);
                listViewHolder.cardFristText.setText(homeDesignBean.getName());
                listViewHolder.cardTowText.setText(homeDesignBean.getBookdesc());
                listViewHolder.cardThreeText.setText(homeDesignBean.getAuthor());
                listViewHolder.cardFourText.setText(homeDesignBean.getKind());
                listViewHolder.titleLayout.setVisibility(Component.HIDE);

                listViewHolder.firstLayout.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        listener.onItemClickListener(component,homeDesignBean);
                    }
                });
                break;
            case ITEM_TYPE_CONTENT:
                recommendViewHolder.bookLayout.setVisibility(Component.HIDE);
                if(contentList.size()> (position-1)*3+1){
                    Glide.with(context)
                            .load(contentList.get((position-1)*3+1).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(recommendViewHolder.mp_home_recommend_firstImage);
                    recommendViewHolder.mp_home_recommend_fristText.setText(contentList.get((position-1)*3+1).getName());
                    recommendViewHolder.firstLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component,contentList.get((position-1)*3+1));
                        }
                    });
                    recommendViewHolder.firstLayout.setVisibility(Component.VISIBLE);
                }else{
                    recommendViewHolder.firstLayout.setVisibility(Component.INVISIBLE);
                }
                if(contentList.size() > (position-1)*3+2){
                    Glide.with(context)
                            .load(contentList.get((position-1)*3+2).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(recommendViewHolder.mp_home_recommend_twoImage);
                    recommendViewHolder.mp_home_recommend_twoText.setText(contentList.get((position-1)*3+2).getName());
                    recommendViewHolder.twoLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component,contentList.get((position-1)*3+2));
                        }
                    });
                    recommendViewHolder.twoLayout.setVisibility(Component.VISIBLE);
                }else{
                    recommendViewHolder.twoLayout.setVisibility(Component.INVISIBLE);
                }
                if(contentList.size() > (position-1)*3+3){
                    Glide.with(context)
                            .load(contentList.get((position-1)*3+3).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(recommendViewHolder.mp_home_recommend_threeImage);
                    recommendViewHolder.mp_home_recommend_threeText.setText(contentList.get((position-1)*3+3).getName());
                    recommendViewHolder.threeLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component,contentList.get((position-1)*3+3));
                        }
                    });
                    recommendViewHolder.threeLayout.setVisibility(Component.VISIBLE);
                }else{
                    recommendViewHolder.threeLayout.setVisibility(Component.INVISIBLE);
                }
                break;
        }

        return component;
    }

    @Override
    public int getItemComponentType(int position) {
        if (position==0) {
            // 最新View
            return ITEM_TYPE_NEW;
        } else {
            // 内容View
            return ITEM_TYPE_CONTENT;
        }
    }

    public class RecommendViewHolder {
        DirectionalLayout bookLayout;
        DirectionalLayout firstLayout;
        DirectionalLayout twoLayout;
        DirectionalLayout threeLayout;
        Image mp_home_recommend_firstImage;
        Image mp_home_recommend_twoImage;
        Image mp_home_recommend_threeImage;
        Text mp_home_recommend_fristText;
        Text mp_home_recommend_twoText;
        Text mp_home_recommend_threeText;
    }

    public class ListViewHolder {
        Image cardTitleImage;

        Text cardFristText;
        Text cardTowText;
        Text cardThreeText;
        Text cardFourText;
        DependentLayout firstLayout;
        DirectionalLayout titleLayout;
        Rating rating;
    }
}


