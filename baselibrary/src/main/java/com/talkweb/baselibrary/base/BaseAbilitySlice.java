package com.talkweb.baselibrary.base;

import com.talkweb.baselibrary.ResourceTable;
import com.talkweb.baselibrary.dialog.LoadingDialog;
import com.talkweb.baselibrary.utils.ResourceHelper;
import com.talkweb.baselibrary.utils.Toast;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.Point;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.Resource;

public abstract class BaseAbilitySlice extends AbilitySlice {

    private LoadingDialog loadingDialog;
    private Intent intent;

    @Override
    protected void onStart(Intent intent) {
        this.intent = intent;
        super.onStart(intent);
        super.setUIContent(getLayoutId());
        initComponent();
        getData();
    }

    public Intent getIntent() {
        return intent;
    }

    protected abstract int getLayoutId();

    protected abstract void initComponent();

    protected abstract void getData();

    public void showToast(Context context, String message) {
        Toast.show(context, message);
    }

    public void showToast(Context context, int messageId) {
        Toast.show(context, getString(messageId));
    }

    public void setTitle(String title) {
        Text text = findById(ResourceTable.Id_title_tv);
        if (text != null) {
            text.setText(title);
        }
    }

    public void setTitle(int resId) {
        Text text = findById(ResourceTable.Id_title_tv);
        if (text != null) {
            text.setText(resId);
        }
    }

    public void showLoadingDialog() {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(this);
        }
        loadingDialog.show();
    }

    public void showLoadingDialog(String loadingTxt) {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(this, loadingTxt);
        }
        loadingDialog.show();
    }

    public void dissmissLoadingDialog() {
        if (loadingDialog != null && loadingDialog.isShowing()) {
            loadingDialog.destroy();
        }
    }

    public int getColor(int colorId) {
        try {
            return getResourceManager().getElement(colorId).getColor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public FractionManager getFractionManager() {
        Ability ability = getAbility();
        if (ability instanceof FractionAbility) {
            FractionAbility fractionAbility = (FractionAbility) ability;
            return fractionAbility.getFractionManager();
        }
        return null;
    }

    //获取状态栏高度
    public float getStatusHeight(Context context) {
        Point point1 = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getSize(point1);

        Point point2 = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getRealSize(point2);
        return point1.getPointY() - point2.getPointY();
    }

    public final String[] getStringArray(int resourceId) {
        return ResourceHelper.getStringArray(this, resourceId);
    }

    public final String getString(int resourceId) {
        return ResourceHelper.getString(this, resourceId);
    }

    public final Resource getResource(int resourceId) {
        return ResourceHelper.getResource(this, resourceId);
    }

    public <T extends Component> T findById(int resID) {
        return (T) findComponentById(resID);
    }

}
