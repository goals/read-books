package com.talkweb.baselibrary.component;

import com.talkweb.baselibrary.utils.AttrSetUtils;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

public class CommonImageText extends DirectionalLayout {

    private Image mImage = null;
    private Text mText = null;
    String text;
    int imageid, textcolor,textsize;

    public CommonImageText(Context context) {
        super(context);
        init(context);
    }

    public CommonImageText(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public CommonImageText(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context);
        attrSet(attrSet);
    }

    private void init(Context context) {
        setOrientation(Component.VERTICAL);
        setAlignment(LayoutAlignment.CENTER);
        if (mImage == null) {
            mImage = new Image(context);
            LayoutConfig layoutConfig = new LayoutConfig();
            layoutConfig.width = LayoutConfig.MATCH_CONTENT;
            layoutConfig.height = LayoutConfig.MATCH_CONTENT;
            mImage.setLayoutConfig(layoutConfig);
        }
        if (mText == null) {
            mText = new Text(context);
            LayoutConfig layoutConfig = new LayoutConfig();
            layoutConfig.width = LayoutConfig.MATCH_CONTENT;
            layoutConfig.height = LayoutConfig.MATCH_CONTENT;
            mText.setLayoutConfig(layoutConfig);
        }

    }

    private void attrSet(AttrSet attrSet){
        imageid = AttrSetUtils.getImageResId(attrSet, "imageid", 0);
        text = AttrSetUtils.getString(attrSet, "text", "");
        textcolor = AttrSetUtils.getColor(attrSet, "textcolor", 0xFF111111);
        textsize = AttrSetUtils.getDimension(attrSet, "textsize", 14);

        this.setText(text);
        mText.setTextAlignment(TextAlignment.CENTER);//字体居中
        mText.setTextSize(textsize);
        this.setTextColor(textcolor);

        if (imageid > 0) {
            try {
                Resource resource = mContext.getResourceManager().getResource(imageid);
                ImageSource imageSource = ImageSource.create(resource, null);
                PixelMap pixelmap = imageSource.createPixelmap(null);
                mImage.setPixelMap(pixelmap);
            } catch (Exception ex) {

            }
        }
        addComponent(mImage);//将图片控件加入到布局中
        addComponent(mText);//将文字控件加入到布局中
    }




    /**
     * 设置显示的文字
     *
     * @param text
     */
    public void setText(String text) {
        this.mText.setText(text);
    }

    /**
     * 设置显示的图片
     *
     * @param resourceID 图片ID
     */
    public void setImgResource(int resourceID) {
        if (resourceID == 0) {
            this.mImage.setPixelMap(0);
        } else {
            this.mImage.setPixelMap(resourceID);
        }
    }

    /**
     * 设置字体颜色(默认为黑色)
     *
     * @param color
     */
    private void setTextColor(int color) {
        if (color == 0) {
            this.mText.setTextColor(Color.BLACK);
        } else {
            this.mText.setTextColor(new Color(color));
        }
    }
}
