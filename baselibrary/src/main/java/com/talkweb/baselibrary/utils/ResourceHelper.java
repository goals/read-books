package com.talkweb.baselibrary.utils;

import ohos.app.Context;
import ohos.global.resource.*;

import java.io.IOException;

public final class ResourceHelper {

    private ResourceHelper() {

    }

    public static String[] getStringArray(Context context, int resourceId) {
        String[] resourceStringArray = new String[]{};
        Element element;
        try {
            ResourceManager resourceManager = context.getResourceManager();
            element = resourceManager.getElement(resourceId);
            resourceStringArray = element.getStringArray();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return resourceStringArray;
    }

    public static String getString(Context context, int resourceId) {
        String resourceString = "";
        Element element;
        try {
            ResourceManager resourceManager = context.getResourceManager();
            element = resourceManager.getElement(resourceId);
            resourceString = element.getString();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return resourceString;
    }

    public static Resource getResource(Context context, int resourceId) {
        Resource resource = null;
        try {
            ResourceManager resourceManager = context.getResourceManager();
            resource = resourceManager.getResource(resourceId);
        } catch (IOException | NotExistException e) {
            e.printStackTrace();
        }
        return resource;
    }
}
