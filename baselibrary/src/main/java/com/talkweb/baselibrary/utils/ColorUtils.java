package com.talkweb.baselibrary.utils;

import ohos.agp.colors.RgbColor;
import ohos.agp.utils.Color;

/**
 * @author : frank
 * @date : 2021/3/19 15:43
 */
public class ColorUtils {

    /**
     * 将颜色值转换为rgb颜色
     *
     * @param color 可为十六进制颜色值或通过context.getColor(resourceId)获取的颜色值，不能直接传入resourceId
     * @return
     */
    public static RgbColor hex2Rgb(int color) {
        System.out.println("传入值：" + color);
        if (color > 0) {
            int r = color & 0x00ff0000 >> 16;
            int g = color & 0x0000ff00 >> 8;
            int b = color & 0x0000ff;
            int a = color >> 24 & 0xf000000;
            System.out.println("alpha" + a + "red" + r + "green" + g + "blue" + b);
            return new RgbColor(r, g, b, a);
        } else {
            Color resultColor = new Color(color);
            return RgbColor.fromArgbInt(resultColor.getValue());
        }

    }

    private static boolean isInColorSpace(int channelValue) {
        return channelValue >= 0 && channelValue <= 255;
    }
}
