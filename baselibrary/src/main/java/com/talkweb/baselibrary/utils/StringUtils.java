package com.talkweb.baselibrary.utils;


import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;


public final class StringUtils {


    private StringUtils() {
    }

    /**
     * 是否为空
     *
     * @param content string
     * @return true if is null
     */
    public static boolean isEmpty(String content) {
        return content == null ||
                content.length() == 0 ||
                content.trim().length() == 0 ||
                "null".equalsIgnoreCase(content);
    }

    /**
     * 是否为空
     *
     * @param list
     * @return
     */
    public static boolean isEmpty(List list) {
        return null == list || list.size() == 0;
    }

    /**
     * 是否为空
     *
     * @param list
     * @return
     */
    public static boolean isEmpty(Object[] list) {
        return null == list || list.length == 0;
    }

    /**
     * 是否为空
     *
     * @param obj
     * @return
     */
    public static boolean isEmpty(Object obj) {
        return obj == null || isEmpty(obj.toString());
    }

    /**
     * 毫秒转换为时分秒
     * HH是24小时制的，hh是12小时
     * setTimeZone用于设置时区，如果不设置就会多出来8小时。
     *
     * @param time
     * @return
     */
    public static String getTime(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        String hms = formatter.format(time);
        return hms;
    }

}
