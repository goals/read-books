package com.talkweb.baselibrary.dialog;

import com.talkweb.baselibrary.utils.ResourceHelper;
import ohos.agp.components.AttrHelper;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;

public abstract class BaseCommonDialog extends CommonDialog {

    private final Context mContext;

    public BaseCommonDialog(Context context) {
        super(context);
        mContext = context;
        initialize(context);
    }

    protected final String getString(int resId) {
        return ResourceHelper.getString(mContext, resId);
    }

    protected final Context getContext() {
        return mContext;
    }

    protected void initialize(Context context) {
        if (isBottomDialog()) {
            Window dialogWindow = getWindow();
            WindowManager.LayoutConfig windowLayoutConfig = dialogWindow.getLayoutConfig().get();
            windowLayoutConfig.y = AttrHelper.vp2px(50, context);
            //将属性设置给窗体
            dialogWindow.setLayoutConfig(windowLayoutConfig);
            dialogWindow.setLayoutAlignment(ohos.agp.utils.LayoutAlignment.BOTTOM);
        }
    }

    protected boolean isBottomDialog() {
        return false;
    }
}
