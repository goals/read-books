package com.talkweb.baselibrary.dialog;

import com.talkweb.baselibrary.ResourceTable;
import com.talkweb.baselibrary.utils.StringUtils;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

public class LoadingDialog extends CommonDialog {

    private String mLoadingText;

    public LoadingDialog(Context context) {
        super(context);
        initialize(context);
    }

    public LoadingDialog(Context context, String text) {
        super(context);
        this.mLoadingText = text;
        initialize(context);
    }


    protected void initialize(Context context) {

        setAutoClosable(true);
        setCornerRadius(AttrHelper.vp2px(8, context));

        //根据屏幕宽度来设置Dialog的宽度，高度自适应
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        final int screenWidth = display.getRealAttributes().width;
        final int width = screenWidth / 3;
        final int height = width;
        setSize(width, height);

        //自定义视图组件的宽高
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig();
        layoutConfig.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
        layoutConfig.height = ComponentContainer.LayoutConfig.MATCH_PARENT;

        DependentLayout customComponent = new DependentLayout(context);
        customComponent.setAlignment(LayoutAlignment.CENTER);
        customComponent.setLayoutConfig(layoutConfig);

        DirectionalLayout.LayoutConfig dlLayoutConfig = new DirectionalLayout.LayoutConfig();
        dlLayoutConfig.width = ComponentContainer.LayoutConfig.MATCH_CONTENT;
        dlLayoutConfig.height = ComponentContainer.LayoutConfig.MATCH_CONTENT;
        DirectionalLayout dlomponent = new DirectionalLayout(context);
        dlomponent.setOrientation(DirectionalLayout.VERTICAL);
        dlomponent.setLayoutConfig(dlLayoutConfig);
        customComponent.addComponent(dlomponent);

        //加载
        DirectionalLayout.LayoutConfig imageConfig = new DirectionalLayout.LayoutConfig();
        imageConfig.width = AttrHelper.vp2px(50, context);
        imageConfig.height = AttrHelper.vp2px(50, context);
        imageConfig.alignment = LayoutAlignment.HORIZONTAL_CENTER;
        Image image = new Image(context);
        image.setPixelMap(ResourceTable.Media_loading);
        image.setLayoutConfig(imageConfig);

        AnimatorProperty animatorProperty = image.createAnimatorProperty();
        animatorProperty.setDuration(2000).setLoopedCount(Integer.MAX_VALUE)
                .setDelay(500).rotate(360).start();
        animatorProperty.setTarget(image);

        //必须添加这个方法，否则加载动画没有效果
        image.setBindStateChangedListener(new Component.BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                if (animatorProperty != null) {
                    animatorProperty.start();
                }
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {

            }
        });

        dlomponent.addComponent(image);

        //提示
        DirectionalLayout.LayoutConfig textConfig = new DirectionalLayout.LayoutConfig();
        textConfig.width = ComponentContainer.LayoutConfig.MATCH_CONTENT;
        textConfig.height = ComponentContainer.LayoutConfig.MATCH_CONTENT;
        textConfig.alignment = LayoutAlignment.HORIZONTAL_CENTER;
        textConfig.setMarginTop(AttrHelper.vp2px(6, context));
        Text text = new Text(context);

        if (!StringUtils.isEmpty(mLoadingText)) {
            text.setText(mLoadingText);
        } else {
            text.setText("载入中...");
        }
        text.setTextAlignment(TextAlignment.CENTER);
        text.setTextColor(Color.GRAY);
        text.setTextSize(AttrHelper.fp2px(12, context));
        text.setLayoutConfig(textConfig);

        dlomponent.addComponent(text);
        setContentCustomComponent(customComponent);

    }

}
