package com.talkweb.baselibrary.bar.component;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.agp.components.AttrSet;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

public class FractionBarComponent extends StackLayout {

    private BarComponentProvider mProvider;
    private int currentPosition;

    public FractionBarComponent(Context context) {
        this(context, null);
    }

    public FractionBarComponent(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public FractionBarComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * 设置适配器
     *
     * @param provider
     */
    public void setProvider(BarComponentProvider provider) {
        if (mProvider != null || provider == null) {
            return;
        }
        currentPosition = -1;
        mProvider = provider;
    }

    /**
     * 设置点前要显示的fraction
     *
     * @param position
     */
    public void setCurrentItem(int position) {
        if (position < 0 || position >= mProvider.getCount()) {
            return;
        }
        if (currentPosition != position) {
            currentPosition = position;
            mProvider.createPageInContainer(this, position);
        }
    }

    public int getCurrentItem() {
        return currentPosition;
    }

    public Fraction getCurrentFraction() {
        if (mProvider == null) {
            return null;
        }
        return mProvider.getCurrentFraction();
    }

    /**
     * 从缓存中获取指定位置的Fraction
     *
     * @param position Fraction的位置
     * @return 如果已缓存该Fraction则直接返回缓存实例，否则返回null
     */
    public Fraction getCacheFractionByPosition(int position) {
        if (mProvider == null) {
            return null;
        }
        return mProvider.getCacheFractionByPosition(position);
    }
}
