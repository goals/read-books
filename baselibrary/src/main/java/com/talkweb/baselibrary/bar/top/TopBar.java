package com.talkweb.baselibrary.bar.top;

import com.talkweb.baselibrary.ResourceTable;
import com.talkweb.baselibrary.bar.common.IBar;
import com.talkweb.baselibrary.utils.StringUtils;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * 顶部导航栏中单个条目的封装，由于目前并不知道泛型的具体类型，所以泛型直接使用？
 */
public class TopBar extends DirectionalLayout implements IBar<TopBarInfo<?>> {

    /**
     * 当前条目所对应的数据
     */
    private TopBarInfo<?> barInfo;
    private Image mImage;
    private Text mBarName;
    private Component mIndicator;

    public TopBar(Context context) {
        this(context, null);
    }

    public TopBar(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public TopBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_bar_top, this, true);
        mImage = (Image) findComponentById(ResourceTable.Id_image);
        mBarName = (Text) findComponentById(ResourceTable.Id_name);
        mIndicator = findComponentById(ResourceTable.Id_indicator);
    }

    @Override
    public void setBarInfo(TopBarInfo<?> info) {
        barInfo = info;
        inflateInfo(false, true);
    }

    private void inflateInfo(boolean selected, boolean init) {
        if (barInfo.barType == TopBarInfo.BarType.IMAGE) {
            if (init) {
                mImage.setVisibility(VISIBLE);
                mBarName.setVisibility(HIDE);
            }
            if (selected) {
                mImage.setPixelMap(barInfo.selectedImage);
                mIndicator.setVisibility(VISIBLE);
            } else {
                mImage.setPixelMap(barInfo.defaultImage);
                mIndicator.setVisibility(HIDE);
            }
        } else if (barInfo.barType == TopBarInfo.BarType.TEXT) {
            if (init) {
                mImage.setVisibility(HIDE);
                mBarName.setVisibility(VISIBLE);
                if (!StringUtils.isEmpty(barInfo.name)) {
                    mBarName.setText(barInfo.name);
                }
            }
            if (selected) {
                mBarName.setTextColor(new Color(parseColor(barInfo.tintColor)));
                mIndicator.setVisibility(VISIBLE);
            } else {
                mBarName.setTextColor(new Color(parseColor(barInfo.defaultColor)));
                mIndicator.setVisibility(HIDE);
            }
        }
    }

    private int parseColor(Object color) {
        if (color instanceof String) {
            return Color.getIntColor((String) color);
        } else {
            return (int) color;
        }
    }

    @Override
    public void resetHeight(int height) {
        LayoutConfig config = (LayoutConfig) getLayoutConfig();
        config.height = height;
        setLayoutConfig(config);
    }

    @Override
    public void onBarSelectedChange(int index, TopBarInfo<?> preInfo, TopBarInfo<?> nextInfo) {
        if (preInfo != barInfo && nextInfo != barInfo) {
            return;
        }
        if (preInfo == nextInfo) {
            return;
        }
        if (preInfo == barInfo) {
            inflateInfo(false, false);
        } else {
            inflateInfo(true, false);
        }
    }

    public TopBarInfo<?> getBarInfo() {
        return barInfo;
    }

    public Image getImage() {
        return mImage;
    }

    public Text getBarName() {
        return mBarName;
    }

    public Component getIndicator() {
        return mIndicator;
    }
}
