package com.talkweb.baselibrary.bar.bottom;

import com.talkweb.baselibrary.bar.common.IBarLayout;
import com.talkweb.baselibrary.utils.DisplayUtils;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * 底部导航栏的封装，第一个泛型就是底部导航栏的条目，第二个泛型就是每个条目的数据
 */
public class BottomNavigationBar extends StackLayout implements IBarLayout<BottomBar, BottomBarInfo<?>> {

    private static final int ID_TAB_BOTTOM = 0XFF;
    /**
     * 事件监听的集合
     */
    private List<OnBarSelectedListener<BottomBarInfo<?>>> tabSelectedListeners = new ArrayList<>();
    /**
     * 当前选中的条目
     */
    private BottomBarInfo<?> selectedInfo;
    /**
     * 底部导航栏的透明度
     */
    private float barBottomAlpha = 1;
    /**
     * 底部导航栏的高度
     */
    private float barBottomHeight = 54;
    /**
     * 底部导航栏线条的高度
     */
    private float barBottomLineHeight = 0.5f;
    /**
     * 底部导航栏线条的颜色
     */
    private RgbColor barBottomLineColor = new RgbColor(223, 224, 225);
    /**
     * 所有的tab
     */
    private List<BottomBarInfo<?>> infoList;

    /**
     * 这里的colorStates状态数组的一维长度必须和后面的colors颜色数组的状态长度一样
     * 总体的意思就是状态和颜色一一对应
     * 如果colorStates[0]=new int[]{ComponentState.COMPONENT_STATE_CHECKED,ComponentState.COMPONENT_STATE_FOCUSED}
     * 那状态必须是checked并且是focused才会显示对应的颜色
     */
    private int[][] colorStates = new int[6][];
    private int[] colors = new int[]{
            Color.getIntColor("#F1F2F4"),
            Color.getIntColor("#F1F2F4"),
            Color.getIntColor("#F1F2F4"),
            Color.getIntColor("#F1F2F4"),
            Color.getIntColor("#FFFFFFFF"),
            Color.getIntColor("#FFFFFFFF"),
    };

    public BottomNavigationBar(Context context) {
        this(context, null);
    }

    public BottomNavigationBar(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public BottomNavigationBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * 根据数据查找条目
     *
     * @param info 条目的数据
     * @return 条目
     */
    @Override
    public BottomBar findBar(BottomBarInfo<?> info) {
        ComponentContainer componentContainer = (ComponentContainer) findComponentById(ID_TAB_BOTTOM);
        for (int i = 0; i < componentContainer.getChildCount(); i++) {
            Component component = componentContainer.getComponentAt(i);
            if (component instanceof BottomBar) {
                BottomBar bottomBar = (BottomBar) component;
                if (bottomBar.getTabInfo() == info) {
                    return bottomBar;
                }
            }
        }
        return null;
    }

    /**
     * 添加监听
     *
     * @param listener 监听
     */
    @Override
    public void addBarSelectedChangeListener(OnBarSelectedListener<BottomBarInfo<?>> listener) {
        tabSelectedListeners.add(listener);
    }

    /**
     * 默认选中的条目
     *
     * @param defaultInfo 默认选中条目的信息
     */
    @Override
    public void defaultSelected(BottomBarInfo<?> defaultInfo) {
        onSelected(defaultInfo);
    }

    /**
     * 初始化所有的条目
     *
     * @param infoList 所有条目的信息
     */
    @Override
    public void initInfo(List<BottomBarInfo<?>> infoList) {
        colorStates[0] = new int[]{ComponentState.COMPONENT_STATE_CHECKED};
        colorStates[1] = new int[]{ComponentState.COMPONENT_STATE_SELECTED};
        colorStates[2] = new int[]{ComponentState.COMPONENT_STATE_PRESSED};
        colorStates[3] = new int[]{ComponentState.COMPONENT_STATE_FOCUSED};
        colorStates[4] = new int[]{ComponentState.COMPONENT_STATE_DISABLED};
        colorStates[5] = new int[]{ComponentState.COMPONENT_STATE_EMPTY};
        if (infoList == null || infoList.isEmpty()) {
            return;
        }
        this.infoList = infoList;
        // 移除之前已经添加的组件，防止重复添加
        removeComponent();
        selectedInfo = null;
        // 添加背景
        addBackground();
        // 添加条目
        addBottomBar();
        // 添加线条
        addBottomLine();
    }

    /**
     * 添加线条
     */
    private void addBottomLine() {
        Component line = new Component(getContext());
        // 目前不支持直接设置背景颜色，只能通过Element来设置背景
        ShapeElement element = new ShapeElement();
        element.setShape(ShapeElement.RECTANGLE);
        element.setRgbColor(barBottomLineColor);
        line.setBackground(element);
        LayoutConfig config = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                DisplayUtils.vp2px(getContext(), barBottomLineHeight));
        // 位于底部
        config.alignment = LayoutAlignment.BOTTOM;
        config.setMarginBottom(DisplayUtils.vp2px(getContext(), barBottomHeight - barBottomLineHeight));
        line.setAlpha(barBottomAlpha);
        addComponent(line, config);
    }

    /**
     * 添加条目
     */
    private void addBottomBar() {
        // 每个条目的宽度就是屏幕宽度除以条目的总个数
//        int width = DisplayUtils.getDisplayWidthInPx(getContext()) / infoList.size();
        // 高度是固定的值，这里需要做屏幕适配，将vp转换成像素
        int height = DisplayUtils.vp2px(getContext(), barBottomHeight);
        DirectionalLayout stackLayout = new DirectionalLayout(getContext());
        stackLayout.setOrientation(Component.HORIZONTAL);
        stackLayout.setId(ID_TAB_BOTTOM);
        for (BottomBarInfo<?> info : infoList) {
            // 创建布局配置对象
            DirectionalLayout.LayoutConfig config = new DirectionalLayout.LayoutConfig(
                    DirectionalLayout.LayoutConfig.MATCH_CONTENT, height);
            // 设置底部对齐
            config.alignment = LayoutAlignment.BOTTOM;
            config.weight = 1;

            // 设置左边距
//            config.setMarginLeft(i * width);
            BottomBar bottomBar = new BottomBar(getContext());
            ShapeElement background = new ShapeElement();
            background.setStateColorList(colorStates, colors);
            //这行代码如果不设置是没有效果的，目前我试了SRC和PLUS有效果的，其他的各位可以自己尝试
            background.setStateColorMode(BlendMode.SRC);
            bottomBar.setBackground(background);
            bottomBar.setLayoutConfig(config);
            tabSelectedListeners.add(bottomBar);
            // 初始化每个条目
            bottomBar.setBarInfo(info);
            // 添加条目
            stackLayout.addComponent(bottomBar, config);
            // 设置点击事件
            bottomBar.setClickedListener(component -> onSelected(info));
        }
        LayoutConfig layoutConfig = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
        layoutConfig.alignment = LayoutAlignment.BOTTOM;
        addComponent(stackLayout, layoutConfig);
    }

    /**
     * 点击条目后给外界回调
     *
     * @param nextInfo 点击后需要选中的条目
     */
    private void onSelected(BottomBarInfo<?> nextInfo) {
        for (OnBarSelectedListener<BottomBarInfo<?>> listener : tabSelectedListeners) {
            listener.onBarSelectedChange(infoList.indexOf(nextInfo), selectedInfo, nextInfo);
        }
        if (nextInfo.tabType == BottomBarInfo.BarType.IMAGE_TEXT) {
            selectedInfo = nextInfo;
        }
    }

    /**
     * 添加背景
     */
    private void addBackground() {
        Component component = new Component(getContext());
        // 目前还不能直接设置背景颜色，只能通过Element来设置背景
        ShapeElement element = new ShapeElement();
        element.setShape(ShapeElement.RECTANGLE);
        RgbColor rgbColor = new RgbColor(255, 255, 255);
        element.setRgbColor(rgbColor);
        component.setBackground(element);
        component.setAlpha(barBottomAlpha);
        LayoutConfig config = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                DisplayUtils.vp2px(getContext(), barBottomHeight));
        config.alignment = LayoutAlignment.BOTTOM;
        addComponent(component, config);
    }

    /**
     * 移除之前已经添加的组件，防止重复添加
     */
    private void removeComponent() {
        for (int i = getChildCount() - 1; i > 0; i--) {
            removeComponentAt(i);
        }
        tabSelectedListeners.removeIf(listener ->
                listener instanceof BottomBar);
    }

    /**
     * 设置底部导航栏的透明度
     *
     * @param barBottomAlpha 底部导航栏的透明度
     */
    public void setBarBottomAlpha(float barBottomAlpha) {
        this.barBottomAlpha = barBottomAlpha;
    }

    /**
     * 设置底部导航栏的高度
     *
     * @param barBottomHeight 底部导航栏的高度
     */
    public void setBarBottomHeight(float barBottomHeight) {
        this.barBottomHeight = barBottomHeight;
    }

    /**
     * 设置底部导航栏线条的高度
     *
     * @param barBottomLineHeight 底部导航栏线条的高度
     */
    public void setBarBottomLineHeight(float barBottomLineHeight) {
        this.barBottomLineHeight = barBottomLineHeight;
    }

    /**
     * 设置底部导航栏线条的颜色
     *
     * @param barBottomLineColor 底部导航栏线条的颜色
     */
    public void setBarBottomLineColor(RgbColor barBottomLineColor) {
        this.barBottomLineColor = barBottomLineColor;
    }
}
