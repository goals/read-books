package com.talkweb.baselibrary.bar.component;

import com.talkweb.baselibrary.bar.top.TopBarInfo;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionManager;

import java.util.List;

public class TopBarComponentProvider extends BarComponentProvider {

    private List<TopBarInfo<?>> mInfoList;

    public TopBarComponentProvider(FractionManager fractionManager, List<TopBarInfo<?>> infoList) {
        super(fractionManager);
        mInfoList = infoList;
    }

    @Override
    public Fraction getFraction(int position) {
        try {
            return (Fraction) mInfoList.get(position).fraction.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getCount() {
        return mInfoList.size();
    }

}
