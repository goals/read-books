package com.talkweb.baselibrary.bar.common;

import com.talkweb.baselibrary.bar.top.TopBarInfo;
import ohos.aafwk.ability.fraction.Fraction;

public class TopBottomBarInfo<Color> {

    /**
     * 默认的图片
     */
    public int defaultImage;
    /**
     * 选中后的图片
     */
    public int selectedImage;
    /**
     * 默认的文字颜色
     */
    public Color defaultColor;
    /**
     * 选中后的文字颜色
     */
    public Color tintColor;

    public String name;
    public TopBarInfo.BarType barType;
    public Class<? extends Fraction> fraction;
}
